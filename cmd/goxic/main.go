package main

import (
	"bytes"
	"flag"
	"fmt"
	"maps"
	"slices"
	"strings"

	"git.fractalqb.de/fractalqb/eloc/must"
	"git.fractalqb.de/fractalqb/goxic"
	"git.fractalqb.de/fractalqb/goxic/goxicweb"
)

var cfg = struct {
	Parser      goxic.Parser
	MaxFixRunes int
}{
	Parser:      *goxicweb.NewParser(),
	MaxFixRunes: 58,
}

func main() {
	flag.Parse()

	for _, arg := range flag.Args() {
		file(arg)
	}
}

func file(name string) {
	fmt.Println("template file:", name)
	tmpls := must.Ret(cfg.Parser.ParseFile(name))
	tnames := slices.Collect(maps.Keys(tmpls))
	slices.Sort(tnames)
	for _, n := range tnames {
		if n != "" {
			fmt.Println("  template:", n)
		}
		t := tmpls[n]
		i := 0
		for numFix := t.NumFix(); i < numFix; i++ {
			if f, _ := t.Field(i); f != "" {
				fmt.Printf("     <%s>\n", f)
			}
			fmt.Printf("  %3d [%s]\n", i, fix(t.Fix(i)))
		}
		if f, _ := t.Field(i); f != "" {
			fmt.Printf("     <%s>\n", f)
		}
	}
}

func fix(f []byte) (res string) {
	rs := bytes.Runes(f)
	if len(rs) > cfg.MaxFixRunes {
		pre := cfg.MaxFixRunes / 2
		res = string(rs[:pre]) + "]…[" + string(rs[len(rs)-cfg.MaxFixRunes+pre:])
	} else {
		res = string(f)
	}
	res = strings.ReplaceAll(res, "\r\n", "↩")
	res = strings.ReplaceAll(res, "\n", "↩")
	res = strings.ReplaceAll(res, "\t", "↦")
	res = strings.ReplaceAll(res, "\f", "↧")
	return res
}
