#!/bin/sh
WATCH=$(ls -1 *.go */*.go)
while inotifywait -e move_self -e modify $WATCH; do
    make cover
done
