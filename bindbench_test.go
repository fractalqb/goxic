package goxic

import (
	"io"
	"maps"
	"slices"
	"strings"
	"testing"
	"time"

	"git.fractalqb.de/fractalqb/eloc/must"
	"git.fractalqb.de/fractalqb/testerr"
)

type benchAccount struct {
	ID        uint32
	Name      string
	EMail     string
	CreatedAt time.Time
	LastSeen  time.Time
}

func (acc *benchAccount) GoxicByName(name string) io.WriterTo {
	switch name {
	case "ID":
		return Int(acc.ID)
	case "Name":
		return Str(acc.Name)
	case "EMail":
		return Str(acc.EMail)
	case "CreatedAt":
		return Time(acc.CreatedAt, time.UnixDate)
	case "LastSeen":
		return Time(acc.LastSeen, time.UnixDate)
	}
	return nil
}

var (
	benchTmpl = must.Ret(NewTemplate(
		"Account ", Field("ID"), ":\n",
		"  Name:      ", Field("Name"), "\n",
		"  EMail:     ", Field("EMail"), "\n",
		"  Created:   ", Field("CreatedAt"), "\n",
		"  Last seen: ", Field("LastSeen"), "\n",
	))

	benchNow = time.Date(2024, time.November, 27, 16, 44, 11, 0, time.UTC)

	benchNameds = slices.Collect(maps.Values(NamesFields(benchTmpl, false)))

	benchData = []benchAccount{
		{
			ID:   1,
			Name: "Luise Müller", EMail: "lieschen@acme.nope",
			CreatedAt: benchNow.Add(-144 * time.Hour),
			LastSeen:  benchNow.Add(-455 * time.Minute),
		},
		{
			ID:   3,
			Name: "Max Mustermann", EMail: "mman@acme.nope",
			CreatedAt: benchNow.Add(-103 * time.Hour),
			LastSeen:  benchNow.Add(-891 * time.Minute),
		},
		{
			ID:   3,
			Name: "John Doe", EMail: "jd@acme.nope",
			CreatedAt: benchNow.Add(-555 * time.Hour),
			LastSeen:  benchNow.Add(-793 * time.Minute),
		},
	}

	benchFieldsMap struct {
		ID        Fields
		Name      Fields
		EMail     Fields
		CreatedAt Fields
		LastSeen  Fields
	}
)

type benchNamedMapT struct {
	ID        Named
	Name      Named
	EMail     Named
	CreatedAt Named
	LastSeen  Named
}

var benchNamedMap benchNamedMapT

func init() {
	must.Do(MapIndices(&benchFieldsMap, benchTmpl, MapAll))
	must.Do(MapIndices(&benchNamedMap, benchTmpl, MapAll))
}

func benchBind(b *Bindings, acc *benchAccount) {
	b.Bind(1, Int(acc.ID))
	b.Bind(2, Str(acc.Name))
	b.Bind(3, Str(acc.EMail))
	b.Bind(4, Time(acc.CreatedAt, time.UnixDate))
	b.Bind(5, Time(acc.LastSeen, time.UnixDate))
}

func benchBindName(b *Bindings, acc *benchAccount) {
	b.BindName("ID", Int(acc.ID))
	b.BindName("Name", Str(acc.Name))
	b.BindName("EMail", Str(acc.EMail))
	b.BindName("CreatedAt", Time(acc.CreatedAt, time.UnixDate))
	b.BindName("LastSeen", Time(acc.LastSeen, time.UnixDate))
}

func benchBindFields(b *Bindings, acc *benchAccount) {
	benchFieldsMap.ID.Bind(b, Int(acc.ID))
	benchFieldsMap.Name.Bind(b, Str(acc.Name))
	benchFieldsMap.EMail.Bind(b, Str(acc.EMail))
	benchFieldsMap.CreatedAt.Bind(b, Time(acc.CreatedAt, time.UnixDate))
	benchFieldsMap.LastSeen.Bind(b, Time(acc.LastSeen, time.UnixDate))
}

func benchBindNamed(b *Bindings, acc *benchAccount) {
	benchNamedMap.ID.Bind(b, Int(acc.ID))
	benchNamedMap.Name.Bind(b, Str(acc.Name))
	benchNamedMap.EMail.Bind(b, Str(acc.EMail))
	benchNamedMap.CreatedAt.Bind(b, Time(acc.CreatedAt, time.UnixDate))
	benchNamedMap.LastSeen.Bind(b, Time(acc.LastSeen, time.UnixDate))
}

func benchBindByName(b *Bindings, acc *benchAccount) {
	benchNamedMap.ID.BindByName(b, acc)
	benchNamedMap.Name.BindByName(b, acc)
	benchNamedMap.EMail.BindByName(b, acc)
	benchNamedMap.CreatedAt.BindByName(b, acc)
	benchNamedMap.LastSeen.BindByName(b, acc)
}

func (bnm *benchNamedMapT) BindAccount(b *Bindings, acc *benchAccount) {
	bnm.ID.BindByName(b, acc)
	bnm.Name.BindByName(b, acc)
	bnm.EMail.BindByName(b, acc)
	bnm.CreatedAt.BindByName(b, acc)
	bnm.LastSeen.BindByName(b, acc)
}

func bindByNameMap(b *Bindings, acc *benchAccount) {
	BindByName(b, benchNameds, acc)
}

var benchBindFuncs = map[string]func(*Bindings, *benchAccount){
	"bind":                benchBind,
	"bind name":           benchBindName,
	"bind fields":         benchBindFields,
	"bind named":          benchBindNamed,
	"bind by name":        benchBindByName,
	"bind by name method": benchNamedMap.BindAccount,
	"bind by name map":    bindByNameMap,
}

func Test_bind(t *testing.T) {
	const expect = `Account 1:
  Name:      Luise Müller
  EMail:     lieschen@acme.nope
  Created:   Thu Nov 21 16:44:11 UTC 2024
  Last seen: Wed Nov 27 09:09:11 UTC 2024
`
	for name, bf := range benchBindFuncs {
		t.Run(name, func(t *testing.T) {
			binds := benchTmpl.NewBindings(nil)
			bf(binds, &benchData[0])
			var sb strings.Builder
			testerr.Shall1(binds.WriteTo(&sb)).BeNil(t)
			if s := sb.String(); s != expect {
				t.Error(s)
			}
		})
	}
}

func Benchmark_bind(b *testing.B) {
	for name, bf := range benchBindFuncs {
		b.Run(name, func(b *testing.B) {
			var binds Bindings
			for i := range b.N {
				acc := &benchData[i%len(benchData)]
				benchTmpl.NewBindings(&binds)
				bf(&binds, acc)
			}
		})
	}
}
