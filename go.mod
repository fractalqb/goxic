module git.fractalqb.de/fractalqb/goxic

go 1.23.3

require (
	git.fractalqb.de/fractalqb/eloc v0.3.0
	git.fractalqb.de/fractalqb/testerr v0.1.1
	github.com/fractalqb/texst v0.9.3
)
