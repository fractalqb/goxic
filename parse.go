package goxic

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"os"
	"strings"
	"unicode"
	"unicode/utf8"
)

type LineComment string

func (le LineComment) Line(line []byte) []byte {
	line = bytes.TrimLeftFunc(line, unicode.IsSpace)
	l := len(line)
	if line = bytes.TrimPrefix(line, []byte(le)); len(line) < l {
		return line
	}
	return nil
}

type BlockComment struct{ start, end []byte }

func NewBlockComment(start, end string) BlockComment {
	return BlockComment{[]byte(start), []byte(end)}
}

func (lb BlockComment) Line(line []byte) []byte {
	line = bytes.TrimSpace(line)
	l := len(line)
	if line = bytes.TrimPrefix(line, lb.start); len(line) == l {
		return nil
	}
	l = len(line)
	if line = bytes.TrimSuffix(line, lb.end); len(line) == l {
		return nil
	}
	return line
}

type Parser struct {
	CommentLine            func(line []byte) []byte
	InlineStart, InlineEnd rune
	FieldName              func(string) error
	Escape                 map[string]Escaper
	PrepLine               func([]byte) []byte
	Newline                string

	istartLen, iendLen int // Len of inline start & end rune in byte
	nl                 []byte
}

func (p *Parser) ParseString(s string) (tmpls map[string]*Template, err error) {
	r := strings.NewReader(s)
	return p.Parse(r)
}

func (p *Parser) ParseFile(name string) (tmpls map[string]*Template, err error) {
	r, err := os.Open(name)
	if err != nil {
		return nil, err
	}
	defer r.Close()
	if tmpls, err = p.Parse(r); err != nil {
		return nil, fmt.Errorf("%s:%w", name, err)
	}
	return tmpls, nil
}

func (p *Parser) ParseFS(fs fs.FS, name string) (tmpls map[string]*Template, err error) {
	r, err := fs.Open(name)
	if err != nil {
		return nil, err
	}
	defer r.Close()
	if tmpls, err = p.Parse(r); err != nil {
		return nil, fmt.Errorf("%s:%w", name, err)
	}
	return tmpls, nil
}

func (p *Parser) Parse(r io.Reader) (tmpls map[string]*Template, err error) {
	tmpls = make(map[string]*Template)
	p.init()
	p.nl = nil
	var (
		ts   []*Template
		path []string
	)
	t := &Template{ValidName: p.FieldName}
	var split scanLines
	scn := bufio.NewScanner(r)
	scn.Split(split.scan)
	lineNo := 0
	for scn.Scan() {
		line := scn.Bytes() // line is ephemeral!
		lineNo++
		if comment := p.CommentLine(line); comment != nil {
			typ, name, noPreNL, noPostNL := parseComType(comment)
			if !noPreNL {
				if err = p.addNL(t); err != nil {
					return nil, fmt.Errorf("%d: %w", lineNo, err)
				}
			}
			switch typ {
			case '>':
				switch {
				case name == "":
					return nil, fmt.Errorf(
						"%d: empty sub-template name",
						lineNo,
					)
				case strings.Contains(name, "/"):
					return nil, fmt.Errorf(
						"%d: sub-template name contains path separator '/'",
						lineNo,
					)
				}
				path = append(path, name)
				pathStr := strings.Join(path, "/")
				if _, ok := tmpls[pathStr]; ok {
					return nil, fmt.Errorf("%d: duplicate sub-template '%s'", lineNo, name)
				}
				ts = append(ts, t)
				t = &Template{ValidName: p.FieldName}
				tmpls[pathStr] = t
				p.prepNL(split.nl, noPostNL)
			case '<':
				lm1 := len(ts) - 1
				if lm1 < 0 {
					return nil, fmt.Errorf("%d: unexpected sub-template end '%s'", lineNo, name)
				}
				if top := path[lm1]; name != top {
					return nil, fmt.Errorf("%d: closing sub-template '%s' in %s",
						lineNo,
						name,
						strings.Join(path, "/"),
					)
				}
				t, ts = ts[lm1], ts[:lm1]
				path = path[:lm1]
				p.prepNL(split.nl, noPostNL)
			case '.':
				if err := p.addField(t, name); err != nil {
					return nil, fmt.Errorf("%d: %w", lineNo, err)
				}
				p.prepNL(split.nl, noPostNL)
			default:
				if err := p.line(t, line); err != nil {
					return nil, fmt.Errorf("%d: %w", lineNo, err)
				}
				p.prepNL(split.nl, false)
			}
		} else {
			if err := p.addNL(t); err != nil {
				return nil, fmt.Errorf("%d: %w", lineNo, err)
			}
			if err := p.line(t, line); err != nil {
				return nil, fmt.Errorf("%d: %w", lineNo, err)
			}
			p.prepNL(split.nl, false)
		}
	}
	if len(ts) > 0 {
		return nil, fmt.Errorf("%d: open nesting: %s", lineNo, strings.Join(path, "/"))
	}
	tmpls[""] = t
	return tmpls, nil
}

func (p *Parser) line(t *Template, line []byte) (err error) {
	if p.PrepLine != nil {
		line = p.PrepLine(line)
	}
	var name []byte
	for len(line) > 0 {
		tok := bytes.IndexRune(line, p.InlineStart)
		switch {
		case tok == utf8.RuneError:
			return errors.New("invalid utf8 rune")
		case tok < 0 || tok+1 >= len(line):
			return t.AppendFix(bytes.Clone(line))
		}
		r, n := utf8.DecodeRune(line[tok+p.istartLen:])
		if r == utf8.RuneError {
			return errors.New("invalid utf8 rune")
		}
		if r == p.InlineStart {
			err = t.AppendFix(bytes.Clone(line[:tok+p.istartLen]))
			if err != nil {
				return err
			}
			line = line[tok+2*n:]
			continue
		}
		t.AppendFix(bytes.Clone(line[:tok]))
		line = line[tok+p.istartLen:]
		tok, name, err = p.name(line, name)
		if err != nil {
			return err
		}
		if err := p.addField(t, string(name)); err != nil {
			return err
		}
		line = line[tok:]
	}
	return nil
}

func (p *Parser) name(line, name []byte) (int, []byte, error) {
	end := 0
	name = name[:0]
	for len(line) > 0 {
		tok := bytes.IndexRune(line, p.InlineEnd)
		switch {
		case tok == utf8.RuneError:
			return 0, nil, errors.New("invalid utf8 rune in name")
		case tok < 0:
			return 0, name, errors.New("unterminated name")
		case tok+1 >= len(line):
			name = append(name, line[:tok]...)
			return end + tok + p.istartLen, name, nil
		}
		s, n := utf8.DecodeRune(line[tok+p.istartLen:])
		if s != p.InlineEnd {
			name = append(name, line[:tok]...)
			return end + tok + p.istartLen, name, nil
		}
		name = append(name, line[:tok+p.istartLen]...)
		end += tok + p.istartLen + n
		line = line[tok+p.istartLen+n:]
	}
	return 0, name, errors.New("unterminated name")
}

func (p *Parser) addField(t *Template, name string) error {
	var esc Escaper
	name, escnm := parseEscaper(name)
	if escnm != "" {
		if p.Escape == nil {
			return fmt.Errorf("unknown escape '%s'", escnm)
		}
		if esc = p.Escape[escnm]; esc == nil {
			return fmt.Errorf("unknown escape '%s'", escnm)
		}
	}
	return t.AppendField(name, esc)
}

func (p *Parser) prepNL(nl []byte, noPostNL bool) {
	if noPostNL {
		p.nl = nil
		return
	}
	if p.Newline == "" {
		p.nl = bytes.Clone(nl)
	} else {
		p.nl = []byte(p.Newline)
	}
}

func (p *Parser) addNL(t *Template) error {
	if p.nl != nil {
		if err := t.AppendFix(p.nl); err != nil {
			return err
		}
		p.nl = nil
	}
	return nil
}

func (p *Parser) init() {
	var r [utf8.UTFMax]byte
	p.istartLen = utf8.EncodeRune(r[:], p.InlineStart)
	p.iendLen = utf8.EncodeRune(r[:], p.InlineEnd)
}

var (
	parseComStart = []byte(">>>")
	parseComEnd   = []byte("<<<")
)

func parseEscaper(name string) (n, e string) {
	idx := strings.LastIndexByte(name, '\\')
	if idx < 0 {
		return name, ""
	}
	return name[:idx], name[idx+1:]
}

// typ is 0, '>', '<' or '.'
func parseComType(line []byte) (typ byte, name string, noPreLN, noPostNL bool) {
	line = bytes.TrimSpace(line)
	if len(line) < 7 {
		return 0, "", false, false
	}
	if line[0] == '\\' {
		noPreLN = true
		line = bytes.TrimSpace(line[1:])
	}
	l := len(line)
	if line[l-1] == '\\' {
		noPostNL = true
		line = bytes.TrimSpace(line[:l-1])
		l = len(line)
	}
	if line = bytes.TrimPrefix(line, parseComEnd); len(line) < l {
		l = len(line)
		if line = bytes.TrimSuffix(line, parseComEnd); len(line) == l {
			return 0, "", false, false
		}
		name = string(bytes.TrimSpace(line))
		return '<', name, noPreLN, noPostNL
	}
	if line = bytes.TrimPrefix(line, parseComStart); len(line) == l {
		return 0, "", false, false
	}
	l = len(line)
	if line = bytes.TrimSuffix(line, parseComStart); len(line) < l {
		name = string(bytes.TrimSpace(line))
		return '>', name, noPreLN, noPostNL
	}
	if line = bytes.TrimSuffix(line, parseComEnd); len(line) < l {
		name = string(bytes.TrimSpace(line))
		return '.', name, noPreLN, noPostNL
	}
	return 0, "", false, false
}

// scanLines is meant to be used with bufio.Scanner
type scanLines struct {
	nl []byte
}

// Based on the std lib code of bufio.ScanLines
func (s *scanLines) scan(data []byte, atEOF bool) (advance int, token []byte, err error) {
	s.nl = s.nl[:0]
	if atEOF && len(data) == 0 {
		return 0, nil, nil
	}
	if i := bytes.IndexByte(data, '\n'); i >= 0 {
		data, cr := dropCR(data[:i])
		if cr {
			s.nl = append(s.nl, '\r', '\n')
		} else {
			s.nl = append(s.nl, '\n')
		}
		return i + 1, data, nil
	}
	if atEOF {
		l := len(data)
		data, cr := dropCR(data)
		if cr {
			s.nl = append(s.nl, '\r')
		}
		return l, data, nil
	}
	return 0, nil, nil
}

// Based on the std lib code of bufio.dropCR
func dropCR(data []byte) ([]byte, bool) {
	if len(data) > 0 && data[len(data)-1] == '\r' {
		return data[0 : len(data)-1], true
	}
	return data, false
}
