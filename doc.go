/*
Package goxic implements the fractal[qb] toxic template engine concept for the
Go programming language.  For details on the idea behind the toxic template
engine see: https://fractalqb.de/toxic/index.html

Designing good programming languages is hard, even for [DSL]s. As a developer, I
also want to minimise the number of DSLs that I have to master. For this reason
alone, it makes sense to control the logic of a template from the host
programming language. In addition, the host language usually provides more
sophisticated tool support – in the language itself, in the IDE and in the
tests.

[DSL]: https://en.wikipedia.org/wiki/Domain-specific_language
*/
package goxic
