package goxic

import (
	"errors"
	"io"
)

type EscapeWriter interface {
	io.Writer
	Close() (int64, error)
}

type Escaper interface {
	Escape(io.Writer) EscapeWriter
}

type EscapeFunc func(io.Writer) EscapeWriter

func (ef EscapeFunc) Escape(w io.Writer) EscapeWriter {
	return ef(w)
}

type EscapeChain []Escaper // TODO better encapsulation

func ChainEscape(es ...Escaper) Escaper {
	var chain EscapeChain
	for _, e := range es {
		switch e := e.(type) {
		case EscapeChain:
			chain = append(chain, e...)
		case nil:
		default:
			chain = append(chain, e)
		}
	}
	switch len(chain) {
	case 0:
		return nil
	case 1:
		return chain[0]
	default:
		return chain
	}
}

func (ec EscapeChain) Escape(w io.Writer) EscapeWriter {
	switch len(ec) {
	case 0:
		return nopClose{w}
	case 1:
		return ec[0].Escape(w)
	}
	res := &chainedWr{EscapeWriter: ec[0].Escape(w)}
	for _, e := range ec[1:] {
		res = &chainedWr{EscapeWriter: e.Escape(res.EscapeWriter), pred: res}
	}
	return res
}

type nopClose struct{ io.Writer }

func (nc nopClose) Close() (int64, error) { return 0, nil }

type chainedWr struct {
	EscapeWriter
	pred *chainedWr
}

func (cw *chainedWr) Close() (n int64, err error) {
	var errs []error
	for cw != nil {
		m, err := cw.EscapeWriter.Close()
		n += m
		if err != nil {
			errs = append(errs, err)
		}
		cw = cw.pred
	}
	switch len(errs) {
	case 0:
		return n, nil
	case 1:
		return n, errs[0]
	default:
		return n, errors.Join(errs...)
	}
}
