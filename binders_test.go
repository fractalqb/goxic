package goxic

import (
	"fmt"
	"io"
	"os"

	"git.fractalqb.de/fractalqb/eloc/must"
)

type testByNamer struct {
	ID   int
	Name string
}

func (tbn *testByNamer) GoxicByName(name string) io.WriterTo {
	switch name {
	case "ID":
		return Int(tbn.ID)
	case "Name":
		return Str(tbn.Name)
	}
	return nil
}

func ExampleNamed_BindByName() {
	tmpl := must.Ret(NewTemplate(
		"User ID : ", Field("ID"), "\n",
		"Username: ", Field("Name"), "\n",
	))
	var idxmap struct {
		*Template
		ID   Named
		Name Named
	}
	must.Do(MapIndices(&idxmap, tmpl, MapAll))

	tbn := testByNamer{
		ID:   4711,
		Name: "John Doe",
	}

	b := idxmap.NewBindings(nil)
	idxmap.ID.BindByName(b, &tbn)
	idxmap.Name.BindByName(b, &tbn)
	b.WriteTo(os.Stdout)

	// Output:
	// User ID : 4711
	// Username: John Doe
}

func ExampleFields_BindOrName() {
	const fieldName = "Einfach eine Überschrift"
	tmpl := must.Ret(NewTemplate("<h1>", Field(fieldName), "</h1>"))
	idxs := NameIndices(tmpl, fieldName)

	b := tmpl.NewBindings(nil)
	fmt.Println("Empty:", b)
	idxs.BindOrName(b, nil)
	fmt.Println("Name :", b)
	idxs.BindOrName(b, Str("Yet Another Heading"))
	fmt.Println("Or   :", b)
	// Output:
	// Empty: <h1></h1>
	// Name : <h1>Einfach eine Überschrift</h1>
	// Or   : <h1>Yet Another Heading</h1>
}
