package goxic

import (
	"errors"
	"fmt"
	"io"
	"path/filepath"
	"strings"
	"testing"

	"git.fractalqb.de/fractalqb/testerr"
	"github.com/fractalqb/texst/texsting"
)

func TestParser(t *testing.T) {
	p := Parser{
		CommentLine: LineComment("#").Line,
		InlineStart: '`', InlineEnd: '`',
		FieldName: func(s string) error {
			if strings.ContainsAny(s, " \t\n\r") {
				return errors.New("name contains space")
			}
			return nil
		},
	}
	var tmpls map[string]*Template
	parse := func(t *testing.T) {
		file := filepath.Join("testdata", t.Name()+".goxic")
		tmpls = testerr.Shall1(p.ParseFile(file)).BeNil(t)
	}
	bind := func(t *testing.T, tmpl *Template) *Bindings {
		b := tmpl.NewBindings(nil)
		for i := range tmpl.NumFields() {
			n, _ := tmpl.Field(i)
			if n == "" {
				if i > 0 && i < tmpl.NumFix() {
					testerr.Should(b.Bind(i, Str(fmt.Sprintf("<#%d>", i)))).BeNil(t)
				}
			} else {
				testerr.Should(b.Bind(i, Str(fmt.Sprintf("[%d:%s]", i, n)))).BeNil(t)
			}
		}
		return b
	}
	test := func(t *testing.T) {
		t.Helper()
		parse(t)
		b := bind(t, tmpls[""])
		texsting.FatalPipe(t, "", func(w io.Writer) {
			testerr.Shall1(b.WriteTo(w)).BeNil(t)
		})
	}
	t.Run("general", test)
	t.Run("endline-field", test)
}

func TestLineEnd(t *testing.T) {
	le := LineComment("//")
	t.Run("OK", func(t *testing.T) {
		com := le.Line([]byte(" 	// this is comment "))
		if s := string(com); s != " this is comment " {
			t.Errorf("comment '%s'", s)
		}
	})
	t.Run("no comment", func(t *testing.T) {
		com := le.Line([]byte(" x	// this is comment "))
		if com != nil {
			t.Errorf("comment '%s'", string(com))
		}
	})
}

func TestLineBlock(t *testing.T) {
	lb := NewBlockComment("/*", "*/")
	t.Run("OK", func(t *testing.T) {
		com := lb.Line([]byte(" 	/* this is comment */ "))
		if s := string(com); s != " this is comment " {
			t.Errorf("comment '%s'", s)
		}
	})
	t.Run("pre", func(t *testing.T) {
		com := lb.Line([]byte(" x	/* this is comment */ "))
		if com != nil {
			t.Errorf("comment '%s'", string(com))
		}
	})
	t.Run("post", func(t *testing.T) {
		com := lb.Line([]byte(" 	/* this is comment */ x "))
		if com != nil {
			t.Errorf("comment '%s'", string(com))
		}
	})
}

func TestParser_name(t *testing.T) {
	p := Parser{InlineStart: '<', InlineEnd: '>'}
	p.init()
	check := func(txt, name string, n int) {
		ni, nn, err := p.name([]byte(txt), nil)
		testerr.Shall(err).BeNil(t)
		if ni != n {
			t.Errorf("next index %d != %d", ni, n)
		}
		if m := string(nn); m != name {
			t.Errorf("name '%s' != '%s'", m, name)
		}
	}
	check("foo>", "foo", 4)
	check("foo> ", "foo", 4)
	check("fo>o", "fo", 3)
	check("fo>>o> ", "fo>o", 6)
}

func Test_parseComType(t *testing.T) {
	test := func(tmpl, name string, typ byte, pre, post bool) {
		t.Helper()
		pt, pn, ppre, ppost := parseComType([]byte(tmpl))
		if pt != typ {
			t.Errorf("[%s] wrong type '%c'", tmpl, pt)
		}
		if pn != name {
			t.Errorf("[%s] wrong name '%s'", tmpl, pn)
		}
		if ppre != pre {
			t.Errorf("[%s] wrong noPreNL %t", tmpl, ppre)
		}
		if ppost != post {
			t.Errorf("[%s] wrong noPostNL %t", tmpl, ppost)
		}
	}

	test("<<< foo >>>", "", 0, false, false)
	test("<<< foo <<<", "foo", '<', false, false)
	test("\\ <<< foo <<<", "foo", '<', true, false)
	test("<<< foo <<< \\", "foo", '<', false, true)
	test("\\ <<< foo <<< \\", "foo", '<', true, true)
	test(">>> foo >>>", "foo", '>', false, false)
	test(">>> foo <<<", "foo", '.', false, false)
}
