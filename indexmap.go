package goxic

import (
	"errors"
	"fmt"
	"reflect"
	"strings"
)

type MapMode uint

const (
	MapAllNamedFields MapMode = (1 << iota)
	MapAllAnonFields
	MapAllIndices

	MapFields   = MapAllNamedFields | MapAllAnonFields
	MapAll      = MapAllNamedFields | MapAllIndices
	MapComplete = MapAll | MapAllAnonFields
)

// MapIndices fills the index map m from the [Template] t. An index map is a
// struct with an anonymous embedded Template pointer and exported fields of
// type [Fields].
func MapIndices(m any, t *Template, mode MapMode) error {
	mv := reflect.Indirect(reflect.ValueOf(m))
	if mv.Kind() != reflect.Struct {
		return fmt.Errorf("cannot map indices to %T", m)
	}
	fnidx := NamesIndices(t, mode&MapAllAnonFields != 0)
	compact(fnidx)
	fnms := make(map[string]bool, len(fnidx))
	for n := range fnidx {
		fnms[n] = true
	}
	if err := mapIdxs(mv, t, mode, fnidx, fnms); err != nil {
		return err
	}
	if len(fnms) > 0 {
		bad := (mode&MapAllAnonFields) != 0 && fnms[""]
		bad = bad || ((mode&MapAllNamedFields) != 0 && (len(fnms) > 1 || !fnms[""]))
		if bad {
			var sb strings.Builder
			sb.WriteString("unmapped fields:")
			for n := range fnms {
				fmt.Fprintf(&sb, " %#v", n)
			}
			return errors.New(sb.String())
		}
	}
	return nil
}

func MultiMapIndices(ts map[string]*Template, mode MapMode, idxMaps map[string]any) error {
	for name, idxMap := range idxMaps {
		t := ts[name]
		if t == nil {
			return fmt.Errorf("no template '%s' for index map", name)
		}
		if err := MapIndices(idxMap, t, mode); err != nil {
			return fmt.Errorf("template '%s': %w", name, err)
		}
	}
	return nil
}

var (
	tmplPtrType = reflect.TypeOf(&Template{})
	fieldsType  = reflect.TypeOf(Fields{})
	namedType   = reflect.TypeOf(Named{})
)

func fieldName(f reflect.StructField) string {
	if tag, ok := f.Tag.Lookup("goxic"); ok {
		return tag
	}
	return f.Name
}

func mapIdxs(mv reflect.Value, t *Template, mode MapMode, fnidx map[string]Fields, fnms map[string]bool) error {
	mt := mv.Type()
	for _, mf := range reflect.VisibleFields(mt) {
		switch {
		case mf.Type == tmplPtrType && t != nil && mf.Anonymous:
			tp := mv.FieldByIndex(mf.Index)
			if !tp.CanSet() {
				return errors.New("cannot set template pointer in index map")
			}
			tp.Set(reflect.ValueOf(t))
		case mf.Type == fieldsType:
			fnm := fieldName(mf)
			if fnm == "-" {
				continue
			}
			if idxs, ok := fnidx[fnm]; ok {
				fv := mv.FieldByIndex(mf.Index)
				if !fv.CanSet() {
					return fmt.Errorf("cannot set map indices for '%s'", fnm)
				}
				fv.Set(reflect.ValueOf(idxs))
				delete(fnms, fnm)
			} else if mode&MapAllIndices != 0 {
				return fmt.Errorf("no field for map indices '%s'", fnm)
			}
		case mf.Type == namedType:
			fnm := fieldName(mf)
			if fnm == "-" {
				continue
			}
			if idxs, ok := fnidx[fnm]; ok {
				fv := mv.FieldByIndex(mf.Index)
				if !fv.CanSet() {
					return fmt.Errorf("cannot set map indices for '%s'", fnm)
				}
				fv.Set(reflect.ValueOf(Named{t: t, idxs: idxs}))
				delete(fnms, fnm)
			} else if mode&MapAllIndices != 0 {
				return fmt.Errorf("no field for map indices '%s'", fnm)
			}
		}
	}
	return nil
}

func compact[M map[K]S, K comparable, S ~[]E, E any](m M) {
	type ks struct {
		k K
		s S
	}
	var count int
	tmp := make([]ks, 0, len(m))
	for k, s := range m {
		count += len(s)
		tmp = append(tmp, ks{k, s})
	}
	pack := make(S, count)
	p := 0
	for _, t := range tmp {
		copy(pack[p:], t.s)
		q := p + len(t.s)
		m[t.k] = pack[p:q:q]
		p = q
	}
}
