package goxic

import (
	"fmt"
	"io"
	"strings"
)

// TODO: It is quite common to bind the same content to more than one field when
// filling in templates. Therefore, fields can be given names and then content
// can be bound to all fields with the same name.
type Bindings struct {
	t  *Template
	bs []io.WriterTo
}

func NewBindings(init []io.WriterTo) *Bindings { return &Bindings{bs: init} }

func (b *Bindings) Template() *Template { return b.t }

func (b *Bindings) NumField() int { return len(b.bs) }

// binding (*Bindings)(nil) is same as binding nil -> [Template.NewBindings]
func (b *Bindings) Bind(field int, content io.WriterTo) error {
	if err := b.checkField(field); err != nil {
		return err
	}
	b.bs[field] = content
	return nil
}

func (b *Bindings) checkField(f int) error {
	if f < 0 || f >= len(b.bs) {
		return fmt.Errorf("bind %d out if range 0...%d", f, len(b.bs)-1)
	}
	return nil
}

// BindOrName binds field i to the name of the field if w is nil. Otherwiese it
// binds w to the field. For an example see [Fields.BindOrName].
func (b *Bindings) BindOrName(field int, content io.WriterTo) error {
	if err := b.checkField(field); err != nil {
		return err
	}
	if content != nil {
		b.bs[field] = content
		return nil
	}
	n := b.t.fdefs[field].name
	b.bs[field] = Str(n)
	return nil
}

func (b *Bindings) Binds(content io.WriterTo, fields ...int) error {
	for _, field := range fields {
		if err := b.checkField(field); err != nil {
			return err
		}
		b.bs[field] = content
	}
	return nil
}

func (b *Bindings) BindAll(content io.WriterTo) {
	for i := range len(b.bs) {
		b.bs[i] = content
	}
}

type UnknownField string

func (err UnknownField) Error() string {
	return fmt.Sprintf("unknown field '%s'", string(err))
}

// BindName is a convenience method that collects the field indices of name and
// then binds all fields to content. When repeatedly binding to the same
// template field, it is more efficient to use [Template.Fields] once and then
// use [Fields.Bind] or [Bindings.Binds] repeatedly. [MapIndices] helps to
// manage templates and their indices in a structured way with so-called index
// maps.
func (b *Bindings) BindName(name string, content io.WriterTo) error {
	if b.t == nil {
		return nil
	}
	count := 0
	for i := range b.t.Fields(name) {
		b.bs[i] = content
		count++
	}
	if count == 0 {
		return UnknownField(name)
	}
	return nil
}

func (bds *Bindings) Binding(field int) io.WriterTo {
	if field < 0 || field >= len(bds.bs) {
		return nil
	}
	return bds.bs[field]
}

type Separator string

func (sep Separator) Join(path, name string) string {
	return strings.Join([]string{path, name}, string(sep))
}

const NamePath = Separator(".")

func (bds *Bindings) Fixate(keepAnon bool, name func(path, name string) string) (*Template, error) {
	if name == nil {
		name = func(_, name string) string { return name }
	}
	res := new(Template)
	if err := res.fixate(bds, nil, keepAnon, "", name); err != nil {
		return nil, err
	}
	return res, nil
}

func (bds *Bindings) WriteTo(w io.Writer) (n int64, err error) {
	l := len(bds.bs)
	if l == 0 {
		return 0, nil
	}
	for i, f := range bds.t.fix {
		m, err := bds.writeBinding(w, i)
		n += m
		if err != nil {
			return n, err
		}
		o, err := w.Write(f)
		n += int64(o)
		if err != nil {
			return n, err
		}
	}
	m, err := bds.writeBinding(w, l-1)
	return n + m, err
}

func (bds *Bindings) String() string {
	var build strings.Builder
	bds.WriteTo(&build)
	return build.String()
}

func (bds *Bindings) Reset() *Bindings {
	clear(bds.bs)
	return bds
}

func (bds *Bindings) writeBinding(w io.Writer, i int) (int64, error) {
	b := bds.bs[i]
	if b == nil {
		return 0, nil
	}
	if e := bds.t.fdefs[i].esc; e != nil {
		ew := e.Escape(w)
		n, err := b.WriteTo(ew)
		if err != nil {
			return n, err
		}
		m, err := ew.Close()
		return n + m, err
	}
	return b.WriteTo(w)
}
