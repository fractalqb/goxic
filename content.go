package goxic

import (
	"fmt"
	"io"
	"iter"
	"strconv"
	"time"
)

var Empty empty

type empty struct{}

func (empty) WriteTo(w io.Writer) (int64, error) { return 0, nil }

type Func func(io.Writer) (int64, error)

func (vf Func) WriteTo(w io.Writer) (int64, error) { return vf(w) }

type Int int64

func (i Int) WriteTo(w io.Writer) (int64, error) {
	n, err := io.WriteString(w, strconv.FormatInt(int64(i), 10))
	return int64(n), err
}

type Uint uint64

func (i Uint) WriteTo(w io.Writer) (int64, error) {
	n, err := io.WriteString(w, strconv.FormatUint(uint64(i), 10))
	return int64(n), err
}

type Bytes []byte

func (b Bytes) WriteTo(w io.Writer) (int64, error) {
	n, err := w.Write([]byte(b))
	return int64(n), err
}

type Str string

func (s Str) WriteTo(w io.Writer) (int64, error) {
	n, err := io.WriteString(w, string(s))
	return int64(n), err
}

func ToStr(s fmt.Stringer) io.WriterTo {
	if s == nil {
		return nil
	}
	return Func(func(w io.Writer) (int64, error) {
		n, err := io.WriteString(w, s.String())
		return int64(n), err
	})
}

func Print(v any) io.WriterTo {
	return Func(func(w io.Writer) (int64, error) {
		n, err := fmt.Fprint(w, v)
		return int64(n), err
	})
}

func Fmt(format string, args ...any) io.WriterTo {
	return Func(func(w io.Writer) (int64, error) {
		n, err := fmt.Fprintf(w, format, args...)
		return int64(n), err
	})
}

func Time(t time.Time, layout string) io.WriterTo {
	return Func(func(w io.Writer) (int64, error) {
		n, err := io.WriteString(w, t.Format(layout))
		return int64(n), err
	})
}

func Slice[S ~[]E, E any](es S, b *Bindings, bind func(int, E, *Bindings)) io.WriterTo {
	return Func(func(w io.Writer) (n int64, err error) {
		for i, e := range es {
			bind(i, e, b)
			m, err := b.WriteTo(w)
			n += m
			if err != nil {
				return n, err
			}
		}
		return n, nil
	})
}

func Seq[S iter.Seq[E], E any](seq S, b *Bindings, bind func(int, E, *Bindings)) io.WriterTo {
	return Func(func(w io.Writer) (n int64, err error) {
		i := 0
		for e := range seq {
			bind(i, e, b)
			m, err := b.WriteTo(w)
			n += m
			if err != nil {
				return n, err
			}
			i++
		}
		return n, nil
	})
}
