package goxicweb

import (
	"bytes"
	"html/template"
	"io"

	"git.fractalqb.de/fractalqb/goxic"
)

var HTMLEscape htmlEscape

type htmlEscape struct{}

func (htmlEscape) Escape(w io.Writer) goxic.EscapeWriter {
	return &escHTML{w: w}
}

type escHTML struct {
	w   io.Writer
	tmp bytes.Buffer
}

func (esc *escHTML) Write(p []byte) (int, error) {
	esc.tmp.Reset()
	template.HTMLEscape(&esc.tmp, p)
	n, err := esc.w.Write(esc.tmp.Bytes())
	return n, err
}

func (esc *escHTML) WriteString(s string) (n int, err error) {
	s = template.HTMLEscapeString(s)
	return io.WriteString(esc.w, s)
}

func (esc *escHTML) Close() (int64, error) { return 0, nil }
