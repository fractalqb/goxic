package goxicweb

import (
	"os"

	"git.fractalqb.de/fractalqb/eloc/must"
	"git.fractalqb.de/fractalqb/goxic"
)

func ExampleHTMLEscape() {
	tmpls := must.Ret(ParseString("Escape `content\\html` here!"))
	b := tmpls[""].NewBindings(nil)
	b.BindName("content", goxic.Str("<b>bold</b>"))
	b.WriteTo(os.Stdout)
	// Output:
	// Escape &lt;b&gt;bold&lt;/b&gt; here!
}
