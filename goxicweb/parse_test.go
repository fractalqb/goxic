package goxicweb

import "testing"

func TestPrepLine(t *testing.T) {
	l := PrepLine([]byte("foo\t "))
	if s := string(l); s != "foo" {
		t.Errorf("1: [%s]", s)
	}
	l = PrepLine([]byte(" \t foo"))
	if s := string(l); s != " foo" {
		t.Errorf("2: [%s]", s)
	}
}
