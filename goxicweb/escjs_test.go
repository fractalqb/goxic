package goxicweb

import (
	"os"

	"git.fractalqb.de/fractalqb/eloc/must"
	"git.fractalqb.de/fractalqb/goxic"
)

func ExampleJSEscape() {
	tmpls := must.Ret(ParseString("Escape `content\\js` here!"))
	b := tmpls[""].NewBindings(nil)
	b.BindName("content", goxic.Str("<b>bold</b>"))
	b.WriteTo(os.Stdout)
	// Output:
	// Escape \u003Cb\u003Ebold\u003C/b\u003E here!
}
