package goxicweb

import (
	"bytes"
	"html/template"
	"io"

	"git.fractalqb.de/fractalqb/goxic"
)

var JSEscape jsEscape

type jsEscape struct{}

func (jsEscape) Escape(w io.Writer) goxic.EscapeWriter {
	return &escJS{w: w}
}

type escJS struct {
	w   io.Writer
	tmp bytes.Buffer
}

func (esc *escJS) Write(p []byte) (int, error) {
	esc.tmp.Reset()
	template.JSEscape(&esc.tmp, p)
	n, err := esc.w.Write(esc.tmp.Bytes())
	return n, err
}

func (esc *escJS) WriteString(s string) (n int, err error) {
	s = template.JSEscapeString(s)
	return io.WriteString(esc.w, s)
}

func (esc *escJS) Close() (int64, error) { return 0, nil }
