package goxicweb

import (
	"bytes"
	"io"
	"io/fs"
	"unicode"
	"unicode/utf8"

	"git.fractalqb.de/fractalqb/goxic"
)

func NewParser() *goxic.Parser {
	return &goxic.Parser{
		CommentLine: goxic.NewBlockComment("<!--", "-->").Line,
		InlineStart: '`',
		InlineEnd:   '`',
		Escape: map[string]goxic.Escaper{
			"html": HTMLEscape,
			"js":   JSEscape,
		},
		PrepLine: PrepLine,
	}
}

func ParseString(s string) (tmpls map[string]*goxic.Template, err error) {
	return defaultParser.ParseString(s)
}

func ParseFile(name string) (tmpls map[string]*goxic.Template, err error) {
	return defaultParser.ParseFile(name)
}

func ParseFS(fs fs.FS, name string) (tmpls map[string]*goxic.Template, err error) {
	return defaultParser.ParseFS(fs, name)
}

func Parse(r io.Reader) (tmpls map[string]*goxic.Template, err error) {
	return defaultParser.Parse(r)
}

func PrepLine(line []byte) []byte {
	if len(line) == 0 {
		return line
	}
	line = bytes.TrimRightFunc(line, unicode.IsSpace)
	i := 0
	for {
		r, n := utf8.DecodeRune(line[i:])
		if r == utf8.RuneError {
			return line
		}
		if !unicode.IsSpace(r) {
			if i > 0 {
				i--
				line[i] = ' '
			}
			return line[i:]
		}
		i += n
	}
}

var defaultParser = *NewParser()
