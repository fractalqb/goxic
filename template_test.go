package goxic

import (
	"testing"

	"git.fractalqb.de/fractalqb/testerr"
)

func TestTemplate_clip(t *testing.T) {
	t.Run("more fix", func(t *testing.T) {
		var tmpl Template
		tmpl.SetField(2, "foo", nil)
		if l := len(tmpl.fix); l != 2 {
			t.Errorf("%d fix, want 1", l)
		}
	})
	t.Run("more field", func(t *testing.T) {
		var tmpl Template
		tmpl.SetFix(1, []byte("foo"))
		if l := len(tmpl.fdefs); l != 3 {
			t.Errorf("%d field, want 3", l)
		}
	})
	t.Run("match", func(t *testing.T) {
		var tmpl Template
		tmpl.SetFix(1, []byte("foo"))
		tmpl.SetField(2, "foo", nil)
		if x, d := len(tmpl.fix), len(tmpl.fdefs); x+1 != d {
			t.Errorf("fix %d / field %d", x, d)
		}
	})
}

func TestTemplate_AppendFix(t *testing.T) {
	t.Run("merge", func(t *testing.T) {
		var tmpl Template
		testerr.Shall(tmpl.AppendFixString("foo")).BeNil(t)
		testerr.Shall(tmpl.AppendFixString("BAR")).BeNil(t)
		if s := string(tmpl.fix[0]); s != "fooBAR" {
			t.Errorf("merged '%s'", s)
		}
	})
	t.Run("append", func(t *testing.T) {
		var tmpl Template
		testerr.Shall(tmpl.AppendFixString("foo")).BeNil(t)
		tmpl.SetField(1, "BAR", nil)
		testerr.Shall(tmpl.AppendFixString("baz")).BeNil(t)
		if s := string(tmpl.fix[0]); s != "foo" {
			t.Fatalf("fix 0 '%s'", s)
		}
		if s := string(tmpl.fix[1]); s != "baz" {
			t.Fatalf("fix 1 '%s'", s)
		}
	})
}

func TestTemplate_AppendField(t *testing.T) {
	t.Run("empty", func(t *testing.T) {
		var tmpl Template
		testerr.Shall(tmpl.AppendField("f", nil)).BeNil(t)
		if l := len(tmpl.fdefs); l != 1 {
			t.Fatalf("%d field defs", l)
		}
		if n := tmpl.fdefs[0].name; n != "f" {
			t.Errorf("field name '%s'", n)
		}
	})
}

func TestTemplate_Pack(t *testing.T) {
	var tmpl Template
	fixs := []string{"foo", "bar", "baz"}
	for i, f := range fixs {
		testerr.Shall(tmpl.SetFixString(i, f)).BeNil(t)
	}
	tmpl.Pack()
	if l := len(tmpl.fix); l != 3 {
		t.Fatal("fix len", l)
	}
	for i, f := range fixs {
		if s := string(tmpl.fix[i]); s != f {
			t.Error("fix", i, s)
		}
	}
}
