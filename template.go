package goxic

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"iter"
	"maps"
	"slices"

	"git.fractalqb.de/fractalqb/eloc"
)

// A template is a sequence of fixed data, preceded, interspersed and followed
// by fields that can be dynamically filled with content. In the simplest case,
// fields and fixed data are addressed by indices. Field 0 is before fix data 0
// and field 1 follows fix data 0 and is before fix data 1 and so on. The last
// part is field N for a template with N fixed data parts:
//
//	[ Field 0 | Fix 0 | Field 1 | Fix 1 | … | Fix N-1 | Field N ]
//
// It is perfectly possible to define templates using its methods. However, it
// is far more convenient to read in text templates from a file using the
// [Parser].
//
// To fill in fields, you must first create a [Bindings] object for the template
// with [Template.NewBindings]. This can then be used to bind content to fields.
// After binding, the complete content is written to an [io.Writer] using
// [Bindings.WriteTo].
//
// A zero Template is ready for use. It must not be copied or changed after
// creating the first [Bindings] for the template.
type Template struct {
	ValidName func(string) error

	fix   [][]byte
	fdefs []fieldDefn
}

func NewTemplate(ff ...any) (*Template, error) {
	t := new(Template)
	if err := t.Append(ff...); err != nil {
		return nil, err
	}
	return t, nil
}

func (t *Template) NumFix() int { return len(t.fix) }

func (t *Template) Fix(i int) []byte {
	if i < 0 || i >= len(t.fix) {
		return nil
	}
	return t.fix[i]
}

// SetFix sets the fix part with index i to fix. The template assumes ownership
// of the memory held by fix. If the caller wants to keep ownership e.g. use
// [bytes.Clone] to pass a copy.
func (t *Template) SetFix(i int, fix []byte) error {
	t.forFixIdx(i)
	t.fix[i] = fix
	return nil
}

func (t *Template) SetFixString(i int, fix string) error {
	return t.SetFix(i, []byte(fix))
}

// AppendFix sets the next fixed part that follows either another fixed part or
// a field, whichever is last. It may assume ownership of the memory held by
// fix.
func (t *Template) AppendFix(fix []byte) error {
	if len(t.fdefs) == 0 {
		return t.SetFix(0, fix)
	}
	l := len(t.fix)
	if t.fdefs[l].name != "" {
		return t.SetFix(l, fix)
	}
	l--
	t.fix[l] = append(t.fix[l], fix...)
	return nil
}

func (t *Template) AppendFixString(fix string) error {
	if len(t.fdefs) == 0 {
		return t.SetFixString(0, fix)
	}
	l := len(t.fix)
	if t.fdefs[l].name != "" {
		return t.SetFixString(l, fix)
	}
	l--
	t.fix[l] = append(t.fix[l], fix...)
	return nil
}

// Field is used with [Template.Append] to build templates.
func Field(name string, esc ...Escaper) appendField {
	return appendField{
		name: name,
		esc:  ChainEscape(esc...),
	}
}

type appendField struct {
	name string
	esc  Escaper
}

func (t *Template) Append(ff ...any) error {
	for _, f := range ff {
		switch f := f.(type) {
		case appendField:
			if err := t.AppendField(f.name, f.esc); err != nil {
				return err
			}
		case []byte:
			if err := t.AppendFix(bytes.Clone(f)); err != nil {
				return err
			}
		case string:
			if err := t.AppendFixString(f); err != nil {
				return err
			}
		case io.WriterTo:
			var tmp bytes.Buffer
			if _, err := f.WriteTo(&tmp); err != nil {
				return err
			}
			if err := t.AppendFix(tmp.Bytes()); err != nil {
				return err
			}
		default:
			return fmt.Errorf("cannot append %T to template", f)
		}
	}
	return nil
}

func (t *Template) NumFields() int { return len(t.fdefs) }

func (t *Template) Field(field int) (string, Escaper) {
	if field < 0 || field >= len(t.fdefs) {
		return "", nil
	}
	fdef := t.fdefs[field]
	return fdef.name, fdef.esc
}

func (t *Template) SetField(field int, name string, esc Escaper) error {
	if err := t.validate(name); err != nil {
		return fmt.Errorf("field %d name: %w", field, err)
	}
	if field < 0 {
		return fmt.Errorf("field index %d < 0", field)
	}
	t.forFieldIdx(field)
	t.fdefs[field] = fieldDefn{name, esc}
	return nil
}

// AppendField sets the next field that follows either another field or a fixed
// part, whichever is last.
func (t *Template) AppendField(name string, esc Escaper) error {
	if name == "" {
		return errors.New("appending anonymous field")
	}
	if len(t.fdefs) == 0 {
		return t.SetField(0, name, esc)
	}
	i := len(t.fix)
	if t.fdefs[i].name != "" {
		i++
	}
	return t.SetField(i, name, esc)
}

// Fields returns the field indices of all fields with the given name.
func (t *Template) Fields(name string) iter.Seq[int] {
	return func(yield func(int) bool) {
		for i, f := range t.fdefs {
			if f.name == name {
				if !yield(i) {
					return
				}
			}
		}
	}
}

func (t *Template) Names() map[string]int {
	uniq := make(map[string]int)
	for _, n := range t.fdefs {
		uniq[n.name]++
	}
	return uniq
}

// ListNames returns a sorted list of field names.
func (t *Template) ListNames(anon bool) (ls []string) {
	ls = slices.Collect(maps.Keys(t.Names()))
	slices.Sort(ls)
	return ls
}

func (t *Template) Static() []byte {
	if t.NumFields() != 2 {
		return nil
	}
	if n, _ := t.Field(0); n != "" {
		return nil
	}
	if n, _ := t.Field(1); n != "" {
		return nil
	}
	if len(t.fix) == 0 {
		return []byte{}
	}
	return t.fix[0]
}

func (t *Template) MustStatic() Bytes {
	if t == nil {
		panic(eloc.New("nil template"))
	}
	b := t.Static()
	if b == nil {
		panic(eloc.New("template is not static"))
	}
	return Bytes(b)
}

// TODO returns reuse unchangeds if t is nil -> tmpls["no-exists"].NewBindins()
func (t *Template) NewBindings(reuse *Bindings) *Bindings {
	if t == nil {
		if reuse == nil {
			reuse = new(Bindings)
		} else {
			*reuse = Bindings{}
		}
		return reuse
	}
	l := len(t.fdefs)
	if reuse == nil {
		return &Bindings{
			t:  t,
			bs: make([]io.WriterTo, l),
		}
	}
	reuse.t = t
	if cap(reuse.bs) < l {
		reuse.bs = make([]io.WriterTo, l)
	} else {
		reuse.bs = reuse.bs[:l]
	}
	return reuse
}

func (t *Template) Pack() {
	if len(t.fix) <= 1 {
		return
	}
	t.fdefs = slices.Clip(t.fdefs)
	t.fix = slices.Clip(t.fix)
	var (
		packed []byte
		se     = make([]int, 0, 2*len(t.fix))
	)
	for _, f := range t.fix {
		se = append(se, len(packed), len(packed)+len(f))
		packed = append(packed, f...)
	}
	for i := range t.fix {
		t.fix[i] = packed[se[2*i]:se[2*i+1]]
	}
}

func (t *Template) validate(name string) error {
	if t.ValidName == nil {
		return nil
	}
	return t.ValidName(name)
}

func (t *Template) forFixIdx(i int) {
	t.fdefs = sliceForIdx(t.fdefs, i+1)
	t.fix = sliceForIdx(t.fix, i)
}

func (t *Template) forFieldIdx(i int) {
	t.fdefs = sliceForIdx(t.fdefs, i)
	t.fix = sliceForIdx(t.fix, i-1)
}

func (t *Template) fixate(b *Bindings, esc Escaper, anon bool, p string, nm func(p, n string) string) error {
	fixField := func(i int) error {
		if i >= len(b.t.fdefs) {
			return nil
		}
		f := b.t.fdefs[i]
		fesc := ChainEscape(esc, f.esc)
		switch bw := b.bs[i].(type) {
		case *Bindings:
			if bw.t != nil {
				n := nm(p, f.name)
				if err := t.fixate(bw, fesc, anon, n, nm); err != nil {
					return err
				}
			}
		case nil:
			if f.name != "" || anon {
				n := nm(p, f.name)
				if err := t.AppendField(n, fesc); err != nil {
					return err
				}
			}
		default:
			var buf bytes.Buffer
			if fesc != nil {
				ew := fesc.Escape(&buf)
				if _, err := bw.WriteTo(ew); err != nil {
					return err
				}
				if _, err := ew.Close(); err != nil {
					return err
				}
			} else if _, err := bw.WriteTo(&buf); err != nil {
				return err
			}
			if err := t.AppendFix(buf.Bytes()); err != nil {
				return err
			}
		}
		return nil
	}
	for i, f := range b.t.fix {
		if err := fixField(i); err != nil {
			return err
		}
		if esc != nil {
			var buf bytes.Buffer
			ew := esc.Escape(&buf)
			if _, err := ew.Write(f); err != nil {
				return err
			}
			if _, err := ew.Close(); err != nil {
				return err
			}
			if err := t.AppendFix(buf.Bytes()); err != nil {
				return err
			}
		} else if err := t.AppendFix(f); err != nil {
			return err
		}
	}
	return fixField(len(b.t.fix))
}

type fieldDefn struct {
	name string
	esc  Escaper
}

func sliceForIdx[S ~[]E, E any](s S, i int) S {
	if l := len(s); i >= l {
		return slices.Grow(s, i+1-l)[:i+1]
	}
	return s
}
