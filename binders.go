package goxic

import (
	"errors"
	"io"
)

type ByNamer interface{ GoxicByName(name string) io.WriterTo }

type ByNameFunc func(name string) io.WriterTo

func (f ByNameFunc) GoxicByName(name string) io.WriterTo { return f(name) }

type Named struct {
	// f.t != nil => len(f.idxs) > 0
	t    *Template
	idxs []int
}

// Template t must not be changed afterwards.
func NameFields(t *Template, name string) (fs Named) {
	if t == nil {
		return
	}
	fs.t = t
	for field := range t.Fields(name) {
		fs.idxs = append(fs.idxs, field)
	}
	return
}

// NamesIndices returns a map of all distinct field names along with their
// respective indices.
//
// Template t must not be changed afterwards.
func NamesFields(t *Template, anon bool) map[string]Named {
	res := make(map[string]Named)
	for field := range t.NumFields() {
		name, _ := t.Field(field)
		if name == "" && !anon {
			continue
		}
		fields := res[name]
		fields.t = t
		fields.idxs = append(fields.idxs, field)
		res[name] = fields
	}
	return res
}

func (f Named) Template() *Template { return f.t }

// TODO Panics if [Named.Template] returns nil?
func (f Named) Name() string {
	fdefs := f.t.fdefs
	return fdefs[f.idxs[0]].name
}

func (f Named) Bind(b *Bindings, content io.WriterTo) error {
	if err := f.check(b); err != nil {
		return err
	}
	// f was created from t => no further checks
	for _, idx := range f.idxs {
		b.bs[idx] = content
	}
	return nil
}

// BindOrName binds fields with their names if content is nil. Otherwise it
// binds w to the fields. See also [Bindings.BindOrName].
func (f Named) BindOrName(b *Bindings, content io.WriterTo) error {
	for _, idx := range f.idxs {
		if err := b.BindOrName(idx, content); err != nil {
			return err
		}
	}
	return nil
}

func (f Named) BindByName(b *Bindings, content ByNamer) error {
	if err := f.check(b); err != nil {
		return err
	}
	// f was created from t => no further checks
	w := content.GoxicByName(f.Name())
	for _, idx := range f.idxs {
		b.bs[idx] = w
	}
	return nil
}

func BindByName(b *Bindings, nmap []Named, content ByNamer) {
	for _, fields := range nmap {
		fields.BindByName(b, content)
	}
}

func (f Named) check(b *Bindings) error {
	if f.Template() == nil {
		return errors.New("binding zero fields")
	}
	if b.Template() != f.Template() {
		return errors.New("binding fields to illegal temlate")
	}
	return nil
}

type Fields []int

// Template t must not be changed afterwards.
func NameIndices(t *Template, name string) (idxs Fields) {
	for i := range t.Fields(name) {
		idxs = append(idxs, i)
	}
	return idxs
}

// NamesIndices returns a map of all distinct field names along with their
// respective indices.
//
// Template t must not be changed afterwards.
func NamesIndices(t *Template, anon bool) map[string]Fields {
	res := make(map[string]Fields)
	for field := range t.NumFields() {
		name, _ := t.Field(field)
		if name == "" && !anon {
			continue
		}
		idxs := res[name]
		res[name] = append(idxs, field)
	}
	return res
}

func (is Fields) Bind(b *Bindings, content io.WriterTo) error {
	return b.Binds(content, is...)
}

// BindOrName binds fields with their names if content is nil. Otherwise it
// binds w to the fields. See also [Bindings.BindOrName].
func (is Fields) BindOrName(b *Bindings, content io.WriterTo) error {
	for _, idx := range is {
		if err := b.BindOrName(idx, content); err != nil {
			return err
		}
	}
	return nil
}
