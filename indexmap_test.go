package goxic

import (
	"maps"
	"os"
	"slices"
	"testing"
)

func ExampleMapIndices() {
	// Explicitly build template, parsing is simpler
	var htmlTmpl Template
	htmlTmpl.AppendFixString("<html>\n")
	htmlTmpl.AppendFixString("  <title>")
	htmlTmpl.AppendField("title", nil) // !No escaper
	htmlTmpl.AppendFixString("</title>\n  <body>\n    <h1>")
	htmlTmpl.AppendField("title", nil) // !No escaper
	htmlTmpl.AppendFixString("</h1>\n")
	htmlTmpl.AppendField("Body", nil) // !No escaper
	htmlTmpl.AppendFixString("\n  </body>\n</html>\n")

	// The index map:
	var htmlMap struct {
		*Template        // Embed the template pointer
		Title     Fields `goxic:"title"` // Set template field name
		Body      Named  // Template field name is the same: "Body"
		Ignore    Fields `goxic:"-"` // Exclude from index mapping
	}
	// Fill htmlMap from template
	MapIndices(&htmlMap, &htmlTmpl, MapAll)

	b := htmlMap.NewBindings(nil)                   // Use the embedded template pointer
	htmlMap.Title.Bind(b, Str("Index Map Example")) // Syntactic sugar b.Binds(Str("…"), htmlMap.Title...)
	htmlMap.Body.Bind(b, Str(`<script>alert("Use Escapers to avoid injection attacks!")</script>`))
	b.WriteTo(os.Stdout)

	// Output:
	// <html>
	//   <title>Index Map Example</title>
	//   <body>
	//     <h1>Index Map Example</h1>
	// <script>alert("Use Escapers to avoid injection attacks!")</script>
	//   </body>
	// </html>
}

func Test_compact(t *testing.T) {
	m := map[string][]int{
		"":    {0, 5},
		"foo": {1, 3},
		"bar": {2},
		"baz": nil,
	}
	n := maps.Clone(m)
	compact(n)
	for k, ms := range m {
		ns := n[k]
		if !slices.Equal(ms, ns) {
			t.Errorf("values %v for %s different to %v", ns, k, ns)
		}
	}
}
