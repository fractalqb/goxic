package goxic

import (
	"io"
	"os"
)

/* NO ERROR HANDLING FOR BREVITY */

// The template to be parsed – often one parses templates from a files
const template = `<!DOCTYPE html>
<html lang="{lang}">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>goxic Example</title>
  </head>
  <body>
    <h1>Personen</h1>
    <table>
<!-- \ >>> rows <<< \ -->
<!-- >>> table-row >>> -->
      <tr>
        <td>{given-name}</td>
        <td>{familyname}</td>
      </tr>
<!-- \ <<< table-row <<< -->
    </table>
  </body>
</html>`

// Index maps for efficient field access for table-rows
var imapRow struct {
	*Template
	GivenName  Fields `goxic:"given-name"`
	Familyname Fields `goxic:"familyname"`
}

// Some data to be used in the template
type person struct {
	givenName  string
	familyname string
}

var persons = []person{
	{"Liese", "Müller"},
	{"Max", "Mustermann"},
}

func Example() {
	// Cerate a simple parser that can handle HTML templates (insecure, no escaping)
	parser := Parser{
		CommentLine: NewBlockComment("<!--", "-->").Line,
		InlineStart: '{', // Repeat runes to put them
		InlineEnd:   '}', // into normal template text.
	}

	// Parse the template; root template gets map key ""
	tmpl, _ := parser.ParseString(template)
	// Initialise index map for table-rows
	MapIndices(&imapRow, tmpl["table-row"], MapAll)

	// Bind root template fields without index map
	rootBind := tmpl[""].NewBindings(nil) // Bindings for root template
	rootBind.BindName("lang", Str("de"))

	rowBind := imapRow.NewBindings(nil) // Bindings for row template (reused)
	// Bind a function that generates all rows
	rootBind.BindName("rows", Func(func(w io.Writer) (n int64, err error) {
		for _, p := range persons {
			imapRow.GivenName.Bind(rowBind, Str(p.givenName))
			imapRow.Familyname.Bind(rowBind, Str(p.familyname))
			m, err := rowBind.WriteTo(w)
			rowBind.Reset()
			n += m
			if err != nil {
				return n, err
			}
		}
		return n, nil
	}))

	// Write template with bindings to stdout
	rootBind.WriteTo(os.Stdout)

	// Output:
	// <!DOCTYPE html>
	// <html lang="de">
	//   <head>
	//     <meta charset="utf-8">
	//     <meta name="viewport" content="width=device-width, initial-scale=1.0">
	//     <title>goxic Example</title>
	//   </head>
	//   <body>
	//     <h1>Personen</h1>
	//     <table>
	//       <tr>
	//         <td>Liese</td>
	//         <td>Müller</td>
	//       </tr>
	//       <tr>
	//         <td>Max</td>
	//         <td>Mustermann</td>
	//       </tr>
	//     </table>
	//   </body>
	// </html>
}
