package goxic

import (
	"io"
	"os"
	"strings"
	"testing"

	"git.fractalqb.de/fractalqb/eloc/must"
	"git.fractalqb.de/fractalqb/testerr"
)

func ExampleBindings() {
	var t Template
	t.SetFix(0, []byte("FIX"))
	t.SetField(0, "foo", nil)
	b := t.NewBindings(nil)
	b.Bind(0, Str("bar"))
	b.Bind(1, Print(4711))
	b.WriteTo(os.Stdout)
	// Output:
	// barFIX4711
}

func ExampleBindings_Fixate() {
	t0 := must.Ret(NewTemplate(
		"x0.0", Field("n0.0"), "x0.1", Field("n0.1", embraceString("[", "]")), "x0.2",
	))
	t1 := must.Ret(NewTemplate(
		"x1.0", Field("n1.0", embraceString("(", ")")), "x1.1", Field("n1.1"), "x1.2",
	))

	b1 := t1.NewBindings(nil)
	must.Do(b1.BindName("n1.1", Str("S1.1")))

	b0 := t0.NewBindings(nil)
	must.Do(b0.BindName("n0.0", Str("S0.0")))
	must.Do(b0.BindName("n0.1", b1))

	tf := must.Ret(b0.Fixate(false, nil))
	bf := tf.NewBindings(nil)
	must.Do(bf.BindName("n1.0", Str("S1.0")))

	bf.WriteTo(os.Stdout)
	// Output:
	// x0.0S0.0x0.1[x1.0][(S1.0)][x1.1][S1.1][x1.2]x0.2
}

func TestBindings_fromNilTemplate(t *testing.T) {
	tmpl := testerr.Shall1(NewTemplate("[[", Field("foo"), "]]")).BeNil(t)
	bt := tmpl.NewBindings(nil)
	testerr.Shall(bt.BindName("foo", (*Template)(nil).NewBindings(nil))).BeNil(t)
	var out strings.Builder
	testerr.Shall1(bt.WriteTo(&out)).BeNil(t)
	if s := out.String(); s != "[[]]" {
		t.Error(s)
	}
	fixt := testerr.Shall1(bt.Fixate(false, nil)).BeNil(t)
	out.Reset()
	testerr.Shall1(fixt.NewBindings(nil).WriteTo(&out)).BeNil(t)
	if s := out.String(); s != "[[]]" {
		t.Error(s)
	}
}

type embrace struct{ Prefix, Postfix []byte }

func embraceString(prefix, postfix string) embrace {
	return embrace{Prefix: []byte(prefix), Postfix: []byte(postfix)}
}

func (e embrace) Escape(w io.Writer) EscapeWriter {
	return &embraceWr{w, e}
}

type embraceWr struct {
	w io.Writer
	embrace
}

func (ev *embraceWr) Write(p []byte) (n int, err error) {
	if ev.Prefix != nil {
		if n, err = ev.w.Write(ev.Prefix); err != nil {
			return n, err
		}
		ev.Prefix = nil
	}
	m, err := ev.w.Write(p)
	return n + m, err
}

func (ev *embraceWr) Close() (int64, error) {
	if ev.w == nil {
		return 0, nil
	}
	n, err := ev.w.Write(ev.Postfix)
	ev.w = nil
	return int64(n), err
}
